const path = require('path')

// login page 
exports.getLoginForm = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views','login.html'))
}

// signup page 
exports.getSignupForm = (req, res)=>{
    res.sendFile(path.join(__dirname,'../','views','signup.html'))
}

// homepage
exports.getHome = (req,res)=>{
    res.sendFile(path.join(__dirname,'../','views', 'dashboard.html'))
}